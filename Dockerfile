# Start with a base image containing Java runtime
FROM java:8

# Make port 7070 available to the world outside this container
EXPOSE 7070

ADD target/session-demo.jar session-demo.jar

# Run the jar file 
ENTRYPOINT ["java","-jar","session-demo.jar"]
