package com.example.demo.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebController {

	@GetMapping
	public String welcomeMessage() {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println("Current time " + dateFormat.format(new Date()));
		return "Demo for Spring boot + GitLab CI/CD + DOCKER + AWS EC2 Update - 1";
	}
}
